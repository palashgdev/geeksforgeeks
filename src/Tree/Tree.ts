import Node from "./Node";

class Tree {
  root: Node;

  constructor(key: number) {
    this.root = new Node(key);
  }

  implement(): void {
    const bt = new Tree(1);

    bt.root.left = new Node(2);

    bt.root.right = new Node(3);

    bt.root.left.left = new Node(4);

    bt.root.left.right = new Node(5);
  }
}

export default Tree;
