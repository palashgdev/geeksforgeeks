class Node {
  left: Node | null;
  right: Node | null;
  key: number;

  constructor(key: number) {
    this.left = null;
    this.right = null;
    this.key = key;
  }
}

export default Node;
