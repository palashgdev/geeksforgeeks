import BinaryTest from "./BinarySearching";

describe("Test for the binary Search", () => {
  it("binary test for empty array", () => {
    const binary = new BinaryTest([]);

    //empty array results no results
    expect(binary.search({ no: 1 })).toBe(-1);
  });

  it("binary test for array", () => {
    const bin = new BinaryTest([1, 2, 3, 4, 5, 6]);

    expect(bin.search({ no: 5 })).toBe(4);
  });
});
