class Merge {
  arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  mergeSort(arr: (number | undefined)[] = this.arr): (number | undefined)[] {
    if (arr.length < 2) {
      return arr;
    }

    const middle = Math.floor(arr.length / 2);

    const leftArraySorted = this.mergeSort(arr.slice(0, middle));
    const rightArraySorted = this.mergeSort(arr.slice(middle, arr.length));

    return this.stich(leftArraySorted, rightArraySorted);
  }

  stich(arr1: (number | undefined)[], arr2: (number | undefined)[]): (number | undefined)[] {
    const result: (number | undefined)[] = [];

    while (arr1.length && arr2.length) {
      if (((arr1[0] as number) < (arr2[0] as number)) as boolean) {
        result.push(arr1.shift());
      } else {
        result.push(arr2.shift());
      }
    }

    while (arr1.length) {
      result.push(arr1.shift());
    }

    while (arr2.length) {
      result.push(arr2.shift());
    }

    return result;
  }
}

export default Merge;
