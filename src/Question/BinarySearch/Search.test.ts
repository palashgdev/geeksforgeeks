import Search from "./Search";

describe("Binary Search", () => {
  it("should able to search", () => {
    const search = new Search([1, 2, 3, 4, 5, 6, 7, 8]);

    expect(search.binarySearchRecursive(0, search.arr.length, 7)).toBe(true);
  });

  it("binary search with -1", () => {
    const search = new Search([1, 2, 3, 4, 5, 6, 7]);

    expect(search.binarySearchRecursive(0, search.arr.length, 10)).toBe(false);
  });

  it("Iterative Binary", () => {
    const search = new Search([1, 2, 3, 4, 5, 6, 7, 8]);

    expect(search.iterativeBinary(0, search.arr.length, 6)).toBe(5);
    expect(search.iterativeBinary(0, search.arr.length, 8)).toBe(7);
  });
});
