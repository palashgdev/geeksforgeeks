class LeftMost {
  private arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  public findLeftIndex(low: number, high: number, number: number): number {
    if (low > high) {
      return -1;
    }

    const mid = Math.floor((low + high) / 2);

    if (this.arr[mid] == number && this.arr[mid - 1] != number) {
      return mid;
    } else if (this.arr[mid] >= number) {
      return this.findLeftIndex(low, mid - 1, number);
    } else {
      return this.findLeftIndex(mid + 1, high, number);
    }
  }
}

export default LeftMost;
