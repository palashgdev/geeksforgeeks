import RightMost from "./RightMost";

describe("RightMost", () => {
  it("Get Right most", () => {
    const arr = [1, 2, 3, 3, 3, 3, 3, 3, 4];

    const rm = new RightMost(arr);

    expect(rm.getRightMost(0, arr.length, 4)).toBe(8);
    expect(rm.getRightMost(0, arr.length, 3)).toBe(7);
  });
});
