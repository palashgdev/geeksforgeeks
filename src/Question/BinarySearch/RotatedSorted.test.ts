import RotatedSorted from "./RotatedSorted";

describe("Rotaed Sorted", () => {
  it("Test case 1 ", () => {
    const arrtest = [1, 2, 3, 4, 5, 6];

    const arr = new RotatedSorted(arrtest);

    expect(arr.check(0, arrtest.length - 1)).toBe(-1);
  });
  it("Test case 2 ", () => {
    const arrtest = [3, 4, 5, 6, 1, 2];

    const arr = new RotatedSorted(arrtest);

    expect(arr.check(0, arrtest.length - 1)).toBe(3);
  });

  it("test case 3", () => {
    const arrtest = [15, 10, 5, 6, 7, 8, 9, 10];

    const arr = new RotatedSorted(arrtest);

    expect(arr.check(0, arrtest.length - 1)).toBe(1);
  });
});
