import LeftMost from "./LeftMost";

describe("Left Most index", () => {
  it("left most", () => {
    const arr = [1, 2, 2, 2, 2, 2, 2];

    const left = new LeftMost(arr);

    expect(left.findLeftIndex(0, arr.length, 2)).toBe(1);
  });
});
