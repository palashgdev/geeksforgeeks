class RightMost {
  private arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  public getRightMost(low: number, high: number, number: number): number {
    if (low > high) {
      return -1;
    }

    const mid = Math.floor((low + high) / 2);

    if (this.arr[mid] === number && this.arr[mid + 1] !== number) {
      return mid;
    } else if (this.arr[mid] > number) {
      return this.getRightMost(low, mid - 1, number);
    } else {
      return this.getRightMost(mid + 1, high, number);
    }
  }
}

export default RightMost;
