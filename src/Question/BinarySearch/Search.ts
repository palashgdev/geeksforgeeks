class Search {
  arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  binarySearchRecursive(low: number, high: number, number: number): boolean {
    if (low > high) {
      return false;
    }
    const mid = Math.floor((low + high) / 2);
    if (this.arr[mid] === number) {
      return true;
    } else if (this.arr[mid] > number) {
      return this.binarySearchRecursive(low, mid - 1, number);
    } else {
      return this.binarySearchRecursive(mid + 1, high, number);
    }
  }

  public iterativeBinary(low: number, high: number, number: number): number {
    while (low <= high) {
      const mid = Math.floor((low + high) / 2);

      if (this.arr[mid] === number) {
        return mid;
      } else if (this.arr[mid] > number) {
        high = mid - 1;
      } else {
        low = mid + 1;
      }
    }

    return -1;
  }
}

export default Search;
