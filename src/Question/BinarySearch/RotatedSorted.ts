class RotatedSorted {
  private arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  check(low: number, high: number): number {
    if (low > high) {
      return -1;
    }

    const mid = Math.floor((low + high) / 2);

    if (mid < high && this.arr[mid] > this.arr[mid + 1]) {
      return mid;
    }

    if (mid > low && this.arr[mid] < this.arr[mid - 1]) {
      return mid - 1;
    }

    if (this.arr[low] >= this.arr[mid]) {
      return this.check(low, mid - 1);
    }

    return this.check(mid + 1, high);
  }
}

export default RotatedSorted;
