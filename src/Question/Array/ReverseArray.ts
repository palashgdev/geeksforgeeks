class ReverseArray {
  arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  private swap(arr: number[], a: number, b: number): void {
    arr[a] = arr[a] ^ arr[b];
    arr[b] = arr[a] ^ arr[b];
    arr[a] = arr[a] ^ arr[b];
  }

  public reverse(a: number, b: number): number[] {
    while (!(a >= b)) {
      //swap arr[a],arr[b]
      this.swap(this.arr, a, b);
      a++;
      b--;
    }

    return this.arr;
  }

  public reversebyd(d: number): number[] {
    this.reverse(0, d - 1);
    this.reverse(d, this.arr.length - 1);
    this.reverse(0, this.arr.length - 1);
    return this.arr;
  }
}

export default ReverseArray;
