class CheckArrayWithGivenSum {
  private arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  // This function is only applicable when the array is sorted
  // a.k.a window sliding
  checkArray(number: number, size: number): boolean {
    if (size > this.arr.length) {
      throw new Error("Not possible");
    }

    let i = 0;

    let sum = 0;
    while (i < size) {
      sum = sum + this.arr[i];
      i++;
    }

    if (sum === number) {
      return true;
    }

    while (i < this.arr.length) {
      //add the next element
      sum = sum + this.arr[i];

      //remove the window last element
      sum = sum - this.arr[i - size];

      if (sum === number) {
        return true;
      }
      i++;
    }

    return false;
  }
}

export default CheckArrayWithGivenSum;
