import SlidingWindow from "./SlidingWindow";

describe("Sliding Window", () => {
  it("case 1", () => {
    const arr = new SlidingWindow([1, 8, 30, -5, 20, 7]);

    expect(arr.maxSumOfConsecutiveArray(3)).toBe(45);
  });

  it("case 2", () => {
    const arr = new SlidingWindow([5, -10, 6, 90, 3]);

    expect(arr.maxSumOfConsecutiveArray(2)).toBe(96);
  });
});
