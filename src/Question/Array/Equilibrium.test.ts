import Equilibrium from "./Equilibium";

describe("Equilibruim test", () => {
  it("case 1 ", () => {
    const qui = new Equilibrium([7, 1, 5, 2, -4, 3, 0]);

    expect(qui.checkEquilibrium()).toEqual({
      found: false,
      at: NaN,
    });
  });

  it("case 2", () => {
    const qui = new Equilibrium([3, 4, 8, -9, 20, 6]);

    expect(qui.checkEquilibrium()).toEqual({
      found: true,
      at: 4,
    });
  });
});
