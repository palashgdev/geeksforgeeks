class SlidingWindow {
  private arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  maxSumOfConsecutiveArray(k: number): number {
    let maxNumber = 0;

    let sum = 0;

    //
    for (let i = 0; i < k; i++) {
      sum = sum + this.arr[i];
    }

    //updating the value of the sum
    maxNumber = sum;

    for (let i = k; i < this.arr.length; i++) {
      sum += this.arr[i];
      sum -= this.arr[i - k];

      //update the sum
      if (sum >= maxNumber) {
        maxNumber = sum;
      }
    }

    return maxNumber;
  }
}

export default SlidingWindow;
