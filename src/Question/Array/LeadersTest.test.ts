import Leader from "./Leader";

describe("Leaders Array", () => {
  it("case 1 ", () => {
    const arr = new Leader([1, 2, 3, 4, 5]);

    expect(arr.getLeaderinArray()).toEqual([5]);
  });

  it("case 2", () => {
    const arr = new Leader([1, 2, 6, 8, 13, 6]);

    expect(arr.getLeaderinArray()).toEqual([6, 13]);
  });
});
