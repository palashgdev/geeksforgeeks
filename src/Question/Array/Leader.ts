class Leader {
  arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  public getLeaderinArray(): number[] {
    const ans: number[] = [];

    let last = this.arr.length - 1;

    let max = this.arr[this.arr.length - 1];

    while (last >= 0) {
      if (this.arr[last] >= max) {
        //update the max and push to array
        max = this.arr[last];
        ans.push(this.arr[last]);
      }
      last--;
    }

    return ans;
  }
}

export default Leader;
