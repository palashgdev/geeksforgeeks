class Equilibrium {
  arr: number[];

  constructor(arr: number[]) {
    this.arr = [...arr];
  }

  /**
   * Method:1
   * O(N2)
   * Compute the left and right sum
   */

  /**
   * Method:2
   * O(N)
   * But the auxillary space will be O(N) and iteration of the array over input will be 3 times
   * Compute the pre-fix sum and suffix sum of the array
   *
   * prefix[i] + suffix[i] = Total Sum + arr[i]
   */

  /**
   * Methods:3
   * O(N) with O(1) Space
   */

  public checkEquilibrium(): { found: boolean; at: number } {
    let sum = 0;

    let leftSum = 0;

    for (let i = 0; i < this.arr.length; i++) {
      sum = sum + this.arr[i];
    }

    for (let i = 0; i < this.arr.length; i++) {
      if (leftSum === sum - this.arr[i]) {
        return {
          found: true,
          at: i,
        };
      }
      leftSum += this.arr[i];
      sum -= this.arr[i];
    }

    return {
      found: false,
      at: NaN,
    };
  }
}
export default Equilibrium;
