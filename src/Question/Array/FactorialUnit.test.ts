import FactorialUnit from "./FactorialUnit";
let fac: FactorialUnit;

describe("Factorial No of Digit", () => {
  beforeEach(() => {
    fac = new FactorialUnit();
  });

  it("No in two's digit", () => {
    expect(fac.noOfDigits(50)).toBe(2);
  });

  it("No in one's Digit", () => {
    expect(fac.noOfDigits(0)).toBe(1);
  });

  it("No in negative digit", () => {
    expect(fac.noOfDigits(-1)).toBe(1);
  });

  it("no of digit in 4", () => {
    expect(fac.noOfDigits(1000)).toBe(4);
  });

  it("Factorial of 2", () => {
    expect(fac.factorial(2)).toBe(2);
  });

  it("Factorial of 3", () => {
    expect(fac.factorial(3)).toBe(6);
  });

  it("digits in the factorial 5", () => {
    expect(fac.factorial(5)).toBe(120);
    expect(fac.noOfDigits(120)).toBe(3);
  });
});
