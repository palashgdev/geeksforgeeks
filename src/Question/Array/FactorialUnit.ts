class FactorialUnit {
  public factorial(no: number): number {
    const arr: { [key: string]: number } = {};

    if (arr[no] === undefined) {
      //factorial is not present
      if (no == 1) {
        return 1;
      }
      const value = no * this.factorial(no - 1);
      // update the value for the memorize
      arr[no] = value;
      return value;
    } else {
      return arr[no];
    }
  }

  public noOfDigits(no: number): number {
    if (no < 0) {
      no = -no;
    }
    return no.toString().length;
  }
}

export default FactorialUnit;
