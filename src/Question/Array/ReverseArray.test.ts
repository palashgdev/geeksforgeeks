import ReverseArray from "./ReverseArray";

describe("Reverse Array", () => {
  it("case 1 ", () => {
    const arr = new ReverseArray([1, 2, 3, 4, 5, 6, 7]);

    expect(arr.reverse(0, arr.arr.length - 1)).toEqual([7, 6, 5, 4, 3, 2, 1]);
  });

  it("case 2", () => {
    const arr = new ReverseArray([1, 2, 3, 4, 5, 6, 7]);

    expect(arr.reversebyd(2)).toEqual([3, 4, 5, 6, 7, 1, 2]);
  });
});
