describe("Set", () => {
  it("Set Type 1", () => {
    const myset = new Set();

    myset.add(1);
    myset.add(3);
    myset.add(3);

    expect(myset).toEqual(new Set([1, 3]));
  });
});
